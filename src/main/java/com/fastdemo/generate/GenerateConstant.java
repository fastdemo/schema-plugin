/**
 * 
 */
package com.fastdemo.generate;

import java.text.SimpleDateFormat;

/**
 * 
 * @date 2018年7月25日
 * @author guxingchun
 */
public class GenerateConstant {
	
	public static final String SWITCH_ON = "true";
	
	public static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

}
