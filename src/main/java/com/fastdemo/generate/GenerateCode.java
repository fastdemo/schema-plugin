package com.fastdemo.generate;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.mybatis.generator.internal.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @goal generateCode
 * @phase process-classes
 * @requiresDependencyResolution process-classes
 * @requiresProject false
 */
public class GenerateCode extends AbstractMojo {
	
	private final Logger logger = LoggerFactory.getLogger(GenerateCode.class);

	@Parameter(defaultValue = "${project.build.directory}", property = "outputDir", required = true)
	private File outputDirectory;

	@Parameter(defaultValue = "${project.build.sourceDirectory}", property = "sourceDirectory", required = true)
	private File sourceDirectory;

	/**
	 * @parameter default-value="${project}"
	 * @required
	 * @readonly
	 */
	private MavenProject mavenProject;

	/**
	 * @parameter expression="${generatorConfig}"
	 * @required
	 */
	private String generatorConfig;

	public void execute() throws MojoExecutionException {

		if (StringUtils.isEmpty(generatorConfig))
			throw new IllegalArgumentException("need to set up 'generatorConfig' configuration of plugin");

		logger.info("project:" + mavenProject);
		generatorCode(GenerateCode.class, generatorConfig);
	}

	public static void printfPath(Class<?> utilClass, String generateConfigRelativePath) {
		String path = utilClass.getResource("").getPath();
		String projectName = path.substring(path.indexOf("/"), path.lastIndexOf("/"));
		String relativelyPath = System.getProperty("user.dir");
		relativelyPath = relativelyPath.replace("\\", "/");
		
		System.out.println("relativelyPath=" + relativelyPath);
		String[] split = relativelyPath.split("/");
		projectName = split[split.length - 1];
		String workspacePath = relativelyPath.substring(0, relativelyPath.indexOf(projectName));

		String genCfg = workspacePath + projectName + generateConfigRelativePath;

		System.out.println("projectName=" + projectName);
		System.out.println("workspacePath=" + workspacePath);
		System.out.println("genCfg=" + genCfg);

	}

	/**
	 * @param mainClass
	 *            main函数所在类
	 * @param generateConfigRelativePath
	 *            purangGeneratorConfig.xml配置文件在项目中的相对路径,例：/src/main/resource/purangGeneratorConfig.xml
	 * @date 2018年1月26日
	 * @author guxingchun
	 */
	public void generatorCode(Class<?> mainClass, String generateConfigRelativePath) {
		String path = mainClass.getResource("").getPath();
		String projectName = path.substring(path.indexOf("/"), path.lastIndexOf("/"));
		String relativelyPath = System.getProperty("user.dir");
		relativelyPath = relativelyPath.replace("\\", "/");
		
		logger.info("relativelyPath=" + relativelyPath);
		String[] split = relativelyPath.split("/");
		projectName = split[split.length - 1];
		String workspacePath = relativelyPath.substring(0, relativelyPath.indexOf(projectName));

		String genCfg = workspacePath + projectName + generateConfigRelativePath;

		
		logger.info("projectName" + projectName);
		logger.info("workspacePath" + workspacePath);
		logger.info("genCfg" + genCfg);
		// ObjectFactory.addResourceClassLoader(Object.class.getClassLoader());
		ObjectFactory.addResourceClassLoader(new ConfigClassLoader(relativelyPath, generatorConfig));
		generatorCode(workspacePath, genCfg);
	}

	public void generatorCode(String workspacePath, String genCfg) {


		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		// String workspace = "../../../../../";
		// String path = GeneratorUtil.class.getResource("").getPath();
		// System.out.println(GeneratorCode.class.getResource("").getPath());
		// String path = GeneratorUtil.class.getResource("").getPath();

		String endPath = "/src/main/java";
		String mapperEndPath = "/src/main/resources";

		File configFile = new File(genCfg);
		// System.out.println("config file path:" + configFile.getPath());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = null;
		try {
			config = cp.parseConfiguration(configFile);

			Context context = config.getContexts().get(0);

			JavaClientGeneratorConfiguration jcgc = context.getJavaClientGeneratorConfiguration();
			jcgc.setTargetProject(workspacePath + jcgc.getTargetProject() + endPath);

			JavaModelGeneratorConfiguration jmgc = context.getJavaModelGeneratorConfiguration();
			jmgc.setTargetProject(workspacePath + jmgc.getTargetProject() + endPath);

			SqlMapGeneratorConfiguration smgc = context.getSqlMapGeneratorConfiguration();
			smgc.setTargetProject(workspacePath + smgc.getTargetProject() + mapperEndPath);

			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLParserException e) {
			e.printStackTrace();
		}

		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = null;
		try {
			myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		try {
			myBatisGenerator.generate(null);
			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
