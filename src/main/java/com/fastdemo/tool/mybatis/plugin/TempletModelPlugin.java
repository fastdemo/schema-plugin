package com.fastdemo.tool.mybatis.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.Plugin;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

import com.fastdemo.generate.GenerateConstant;
import com.fastdemo.tool.mybatis.java.GenerateBaseBean;
import com.fastdemo.tool.mybatis.java.JavaFileGenerated;
import com.fastdemo.tool.mybatis.java.impl.ControllerGenerated;
import com.fastdemo.tool.mybatis.java.impl.DTOGenerated;
import com.fastdemo.tool.mybatis.java.impl.ServiceGenerated;
import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethodGenerated;

public class TempletModelPlugin extends PluginAdapter {

	private List<MethodGenerated> methodGenerateds = new ArrayList<MethodGenerated>();

	private List<JavaFileGenerated> javaFileGenerateds = new ArrayList<JavaFileGenerated>();

	private List<String> oMethodList = new ArrayList<String>();

	public static List<String> oFieldList = new ArrayList<String>();

	private List<String> javaDocLines = new ArrayList<String>();

	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String version = "V1.0";

	public static List<String> getFieldList() {
		return oFieldList;
	}

	public TempletModelPlugin() {
		super();

		oFieldList.add("id");
		oFieldList.add("createTime");
		oFieldList.add("updateTime");
		oFieldList.add("createUser");
		oFieldList.add("updateUser");
		oFieldList.add("createUserName");
		oFieldList.add("updateUserName");
		oFieldList.add("isDelete");
		oFieldList.add("version");

		oMethodList.add("getId");
		oMethodList.add("setId");
		oMethodList.add("getCreateTime");
		oMethodList.add("setCreateTime");
		oMethodList.add("getUpdateTime");
		oMethodList.add("setUpdateTime");
		oMethodList.add("getCreateUser");
		oMethodList.add("setCreateUser");
		oMethodList.add("getUpdateUser");
		oMethodList.add("setUpdateUser");
		oMethodList.add("getCreateUserName");
		oMethodList.add("setCreateUserName");
		oMethodList.add("getUpdateUserName");
		oMethodList.add("setUpdateUserName");
		oMethodList.add("getIsDelete");
		oMethodList.add("setIsDelete");
		oMethodList.add("getVersion");
		oMethodList.add("setVersion");

		String property;
		for (GennerageMethod method : GennerageMethod.values()) {
			property = context.getProperty("generageMethod_" + method.getMapperName());
			if(GenerateConstant.SWITCH_ON.equals(property))
				method.setGenerate(true);
		}
		
		for (GennerageMethod method : GennerageMethod.values()) {
			if(method.isGenerate())
				methodGenerateds.add(method.getTargetClass());
		}

		String str = df.format(new Date());
		String currentUser = System.getProperty("user.name");
		javaDocLines.add("/**");
		javaDocLines.add(" * @Description auto create by dec-tool-" + version);
		javaDocLines.add(" * ");
		javaDocLines.add(" * @author " + currentUser);
		javaDocLines.add(" * @date " + str);
		javaDocLines.add(" * ");
		javaDocLines.add(" */");
	}

	@Override
	public void initialized(IntrospectedTable introspectedTable) {
		if (GenerateConstant.SWITCH_ON.equals(context.getProperty("generateService"))) {
			javaFileGenerateds.add(new ServiceGenerated());
		}
		if (GenerateConstant.SWITCH_ON.equals(context.getProperty("generateController"))) {
			javaFileGenerateds.add(new ControllerGenerated());
		}
		if (GenerateConstant.SWITCH_ON.equals(context.getProperty("generateDTO"))) {
			javaFileGenerateds.add(new DTOGenerated());
		}
	}

	@Override
	public boolean validate(List<String> arg0) {
		return true;
	}

	@Override
	public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		String fieldName = field.getName();
		if (oFieldList.contains(fieldName)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean modelGetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		String methodName = method.getName();
		if (oMethodList.contains(methodName)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		String methodName = method.getName();
		if (oMethodList.contains(methodName)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		String baseEntity = context.getProperty("baseEntity");

		FullyQualifiedJavaType superEntityType = new FullyQualifiedJavaType(baseEntity);
		topLevelClass.setSuperClass(superEntityType);

		topLevelClass.addImportedType(superEntityType);
		for (String line : javaDocLines) {
			topLevelClass.addJavaDocLine(line);
		}
		Field field = new Field();
		field.setName("serialVersionUID");
		field.setStatic(true);
		field.setFinal(true);
		field.setInitializationString("1L");
		field.setType(new FullyQualifiedJavaType("long"));
		field.setVisibility(JavaVisibility.PRIVATE);
		field.addJavaDocLine("");
		field.addJavaDocLine("/**");
		field.addJavaDocLine(" * SerialVersion");
		field.addJavaDocLine(" */");
		topLevelClass.getFields().add(0, field);
		return true;
	}

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {

		GenerateBaseBean.baseBeanCopyToTarget(context.getProperty("generateBase"), context.getProperty("systemPackage"),
				context.getJavaClientGeneratorConfiguration().getTargetProject());
		List<GeneratedJavaFile> gjfs = new ArrayList<GeneratedJavaFile>();
		for (JavaFileGenerated javaFileGenerated : javaFileGenerateds) {
			List<GeneratedJavaFile> rgjfs = javaFileGenerated.contextGenerateAdditionalJavaFiles(introspectedTable, context, javaDocLines);
			if (rgjfs != null && rgjfs.size() > 0) {
				gjfs.addAll(rgjfs);
			}
		}
		return gjfs;
	}

	@Override
	public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		String baseMapper = context.getProperty("baseMapper");
		// String annotationMapperClass = context.getProperty("annotationMapperClass");
		// String annotationMapper = context.getProperty("annotationMapper");

		interfaze.getMethods().clear();

		FullyQualifiedJavaType superInterfaceType = new FullyQualifiedJavaType(baseMapper);
		interfaze.addImportedType(superInterfaceType);

		FullyQualifiedJavaType entityType = introspectedTable.getRules().calculateAllFieldsClass();

		superInterfaceType.addTypeArgument(entityType);

		interfaze.getSuperInterfaceTypes().add(superInterfaceType);
		
		// FullyQualifiedJavaType annotationMapperType = new FullyQualifiedJavaType(
		// annotationMapperClass);
		// interfaze.addImportedType(annotationMapperType);
		// interfaze.addAnnotation(annotationMapper);

		for (String line : javaDocLines) {
			interfaze.addJavaDocLine(line);
		}
		return true;
	}

	@Override
	public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
		for (MethodGenerated clientGenerated : methodGenerateds) {
			clientGenerated.generatedSql(document, introspectedTable, context);
		}
		XmlElement parentElement = document.getRootElement();
		List<Element> oldElements = new ArrayList<Element>();
		oldElements.addAll(parentElement.getElements());
		parentElement.getElements().clear();
		parentElement.getElements().add(new TextElement(""));
		for (Element element : oldElements) {
			parentElement.getElements().add(element);
			parentElement.addElement(new TextElement(""));
		}
		return true;
	}

	@Override
	public boolean sqlMapInsertElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		List<IntrospectedColumn> IntrospectedColumnList = introspectedTable.getPrimaryKeyColumns();
		if (IntrospectedColumnList.size() == 1) {
			// IntrospectedColumn introspectedColumn = IntrospectedColumnList.get(0);
			// XmlElement answer = new XmlElement("selectKey");
			// answer.addAttribute(new Attribute("order", "BEFORE"));
			// answer.addAttribute(new Attribute("resultType", "string"));
			// answer.addAttribute(new Attribute("keyProperty",
			// introspectedColumn.getJavaProperty()));
			// answer.addElement(new TextElement("SELECT (" +
			// context.getProperty("generateIdSql") + ")"));
			// element.addElement(0, answer);
		}
		return false;
	}

	@Override
	public boolean sqlMapInsertSelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		// List<IntrospectedColumn> IntrospectedColumnList =
		// introspectedTable.getPrimaryKeyColumns();
		// if (IntrospectedColumnList.size() == 1) {
		// IntrospectedColumn introspectedColumn = IntrospectedColumnList.get(0);
		// XmlElement answer = new XmlElement("selectKey");
		// answer.addAttribute(new Attribute("order", "BEFORE"));
		// answer.addAttribute(new Attribute("resultType", "string"));
		// answer.addAttribute(new Attribute("keyProperty",
		// introspectedColumn.getJavaProperty()));
		// answer.addElement(new TextElement("SELECT (" +
		// context.getProperty("generateIdSql") + ")"));
		// element.addElement(0, answer);
		// }
		resetId(element, "insert");
		return true;
	}

	private void resetId(XmlElement element, String newId) {
		List<Attribute> attributes = element.getAttributes();
		Attribute idAttribute = null;
		for (Attribute attribute : attributes) {
			if (attribute.getName().equals("id")) {
				idAttribute = attribute;
			}
		}
		attributes.remove(idAttribute);
		attributes.add(0, new Attribute("id", newId));
	}

	public boolean sqlMapSelectByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		// resetId(element, "selectById");
		return true;
	}

	public boolean sqlMapDeleteByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		// resetId(element, "deleteById");
		return true;
	}

	public boolean sqlMapUpdateByPrimaryKeySelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		// resetId(element, "updateById");
		return true;
	}

	public boolean sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return false;
	}

}
