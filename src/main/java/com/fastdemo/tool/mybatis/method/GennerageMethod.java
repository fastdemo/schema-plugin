/**
 * 
 */
package com.fastdemo.tool.mybatis.method;

import com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated;
import com.fastdemo.tool.mybatis.method.impl.DeleteBatchByIdGenerate;
import com.fastdemo.tool.mybatis.method.impl.DeleteBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.InsertBatchGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectByIdsGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectCountGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectIdBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateBatchByIdGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateByIdsGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateBySelectiveGenerate;

/**
 * 
 * @date 2018年7月24日
 * @author guxingchun
 */
public enum GennerageMethod {

	INSERT_BATCH("insertBatch"),
	DELETE_BATCH_BY_ID("deleteBatchById"),
	DELETE_BY_SELECTIVE("deleteBySelective"),
	UPDATE_BATCH_BY_ID("updateBatchById"),
	UPDATE_BY_IDS("updateByIds"),
	UPDATE_BY_SELECTIVE("updateBySelective"),
	SELECT_BY_IDS("selectByIds"),
	SELECT_BY_SELECTIVE("selectBySelective"),
	SELECT_COUNTBY_SELECTIVE("selectCountBySelective"),
	SELECT_ID_BY_SELECTIVE("selectIdBySelective"),

	;

	static {
		try {
			INSERT_BATCH.setTargetClass(InsertBatchGenerate.class.newInstance());
			DELETE_BATCH_BY_ID.setTargetClass(DeleteBatchByIdGenerate.class.newInstance());
			DELETE_BY_SELECTIVE.setTargetClass(DeleteBySelectiveGenerate.class.newInstance());
			UPDATE_BATCH_BY_ID.setTargetClass(UpdateBatchByIdGenerate.class.newInstance());
			UPDATE_BY_IDS.setTargetClass(UpdateByIdsGenerate.class.newInstance());
			UPDATE_BY_SELECTIVE.setTargetClass(UpdateBySelectiveGenerate.class.newInstance());
			SELECT_BY_IDS.setTargetClass(SelectByIdsGenerate.class.newInstance());
			SELECT_BY_SELECTIVE.setTargetClass(SelectBySelectiveGenerate.class.newInstance());
			SELECT_COUNTBY_SELECTIVE.setTargetClass(SelectCountGenerate.class.newInstance());
			SELECT_ID_BY_SELECTIVE.setTargetClass(SelectIdBySelectiveGenerate.class.newInstance());
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		INSERT_BATCH.setListParameter(true);
		DELETE_BATCH_BY_ID.setListParameter(true);
		DELETE_BY_SELECTIVE.setListParameter(true);
		UPDATE_BATCH_BY_ID.setListParameter(true);
		UPDATE_BY_IDS.setListParameter(true);
		UPDATE_BY_SELECTIVE.setListParameter(true);
		SELECT_BY_IDS.setListParameter(true);
		SELECT_BY_SELECTIVE.setListParameter(true);
		SELECT_COUNTBY_SELECTIVE.setListParameter(true);
		SELECT_ID_BY_SELECTIVE.setListParameter(true);
	}

	private GennerageMethod(String mapperName) {
		this.mapperName = mapperName;
	}

	private String mapperName;

	private AbstractClientGenerated targetClass;

	private boolean isGenerate = false;
	
	private boolean listParameter = false;

	public String getMapperName() {
		return mapperName;
	}

	public AbstractClientGenerated getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(AbstractClientGenerated targetClass) {
		this.targetClass = targetClass;
	}

	public boolean isGenerate() {
		return isGenerate;
	}

	public void setGenerate(boolean isGenerate) {
		this.isGenerate = isGenerate;
	}

	public boolean isListParameter() {
		return listParameter;
	}

	public void setListParameter(boolean listParameter) {
		this.listParameter = listParameter;
	}

}
