package com.fastdemo.tool.mybatis.method.impl;

import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.internal.types.JdbcTypeNameTranslator;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class UpdateBySelectiveGenerate extends AbstractClientGenerated {

	@Override
	public boolean generatedMethod(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document, IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();

		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.UPDATE.getMethodTag());

		answer.addAttribute(new Attribute("id", GennerageMethod.UPDATE_BY_SELECTIVE.getMapperName()));

		String parameterType = introspectedTable.getBaseRecordType();
//		String parameterType = "map";

		answer.addAttribute(new Attribute("parameterType", parameterType));

		StringBuilder sb = new StringBuilder();
		sb.append("update ");
		sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		answer.addElement(new TextElement(sb.toString()));

		XmlElement set = new XmlElement("set");
		answer.addElement(set);
		List<IntrospectedColumn> introspectedColumns = introspectedTable.getNonPrimaryKeyColumns();
		for (IntrospectedColumn introspectedColumn : introspectedColumns) {
			String beanPropertyName = introspectedColumn.getJavaProperty();
			String columnName = introspectedColumn.getActualColumnName();
			String jdbcType = JdbcTypeNameTranslator.getJdbcTypeName(introspectedColumn.getJdbcType());
			XmlElement ifElement = new XmlElement("if");
			Attribute ifAttribute = new Attribute("test", "data." + beanPropertyName + " != null");
			ifElement.addAttribute(ifAttribute);
			ifElement.addElement(new TextElement(columnName + " = #{data." + beanPropertyName + ",jdbcType=" + jdbcType + "},"));
			set.addElement(ifElement);
		}

		XmlElement where = buildWhereElement(introspectedTable, context);

		answer.addElement(where);
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.UPDATE;
	}

}
