/**
 * 
 */
package com.fastdemo.tool.mybatis.method;

/**
 * 
 * @date 2018年7月25日
 * @author guxingchun
 */
public enum MethonType {

	SELECT, DELETE, UPDATE, INSERT, SELECT_ONE;
	
	
	public String getMethodTag() {
		if(this.equals(SELECT_ONE))
			return SELECT.name().toLowerCase();
		return this.name().toLowerCase();
	}
	
}
