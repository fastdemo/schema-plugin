package com.fastdemo.tool.mybatis.method.impl;

import java.util.Iterator;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;
import org.mybatis.generator.config.Context;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class SelectBySelectiveGenerate extends AbstractClientGenerated {

	@Override
	public boolean generatedMethod(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document, IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();

		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.SELECT.getMethodTag()); //$NON-NLS-1$

		answer.addAttribute(new Attribute("id", GennerageMethod.SELECT_BY_SELECTIVE.getMapperName())); //$NON-NLS-1$
		answer.addAttribute(new Attribute("resultMap", //$NON-NLS-1$
				introspectedTable.getBaseResultMapId()));

		answer.addAttribute(new Attribute("parameterType", //$NON-NLS-1$
				introspectedTable.getBaseRecordType()));

		StringBuilder sb = new StringBuilder();
		sb.append("select "); //$NON-NLS-1$

		if (introspectedTable.hasBaseColumns()) {
			answer.addElement(new TextElement(sb.toString()));
			answer.addElement(getBaseColumnListElement(introspectedTable));
		} else {

			Iterator<IntrospectedColumn> iter = introspectedTable.getAllColumns().iterator();
			while (iter.hasNext()) {
				sb.append(MyBatis3FormattingUtilities.getSelectListPhrase(iter.next()));

				if (iter.hasNext()) {
					sb.append(", "); //$NON-NLS-1$
				}

				if (sb.length() > 80) {
					answer.addElement(new TextElement(sb.toString()));
					sb.setLength(0);
				}
			}
			answer.addElement(new TextElement(sb.toString()));
		}

		sb.setLength(0);
		sb.append("from "); //$NON-NLS-1$
		sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		answer.addElement(new TextElement(sb.toString()));

		XmlElement where = buildWhereElement(introspectedTable, context);

		XmlElement orderAndDirIf = new XmlElement("if");
		orderAndDirIf.addAttribute(new Attribute("test", "orderColumn != null and orderDir != null"));
		orderAndDirIf.addElement(new TextElement("ORDER BY ${orderColumn} ${orderDir}"));

		XmlElement orderIf = new XmlElement("if");
		orderIf.addAttribute(new Attribute("test", "orderColumn != null and orderDir == null"));
		orderIf.addElement(new TextElement("ORDER BY ${orderColumn}"));

		XmlElement defaultOrderIf = new XmlElement("if");
		defaultOrderIf.addAttribute(new Attribute("test", "orderColumn == null"));
		defaultOrderIf.addElement(new TextElement("ORDER BY create_time DESC"));

		XmlElement pageIf = new XmlElement("if");
		pageIf.addAttribute(new Attribute("test", "start != null and length != null"));
		pageIf.addElement(new TextElement(" limit #{start},#{length}"));

		answer.addElement(where);
		answer.addElement(orderAndDirIf);
		answer.addElement(orderIf);
		answer.addElement(defaultOrderIf);
		answer.addElement(pageIf);
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.SELECT;
	}
}
