package com.fastdemo.tool.mybatis.java;

import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.config.Context;

public interface JavaFileGenerated {

	public static final String METHOD_ADD = "add";

	public static final String METHOD_ADD_LIST = "addList";

	public static final String METHOD_SAVE = "save";

	public static final String METHOD_DELETE = "delete";

	public static final String METHOD_DELETE_LIST = "deleteList";

	public static final String METHOD_UPDATE = "update";

	public static final String METHOD_QUERY = "query";

	public static final String METHOD_QUERY_LIST = "List";

	public static final String METHOD_QUERY_PAGE = "Page";

	public static final String METHOD_QUERY_COUNT = "Count";
	
	public static final String METHOD_QUERY_DATATABLE = "DataTable";

	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable, Context context, List<String> javaDocLines);

}
