package com.fastdemo.generatetemplet.dao;

import java.util.List;

import com.fastdemo.generatetemplet.entity.BaseEntity;

public interface BaseMapper<T extends BaseEntity> {
	
	/**
	 * 新增
	 * 
	 * @param entity
	 * @return
	 */
	int insert(T entity);
	
	/**
	 * 根据主键删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteByPrimaryKey(String id);

	/**
	 * 条件删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteBySelective(T entity);

	/**
	 * 条件主键更新
	 * 
	 * @param entity
	 * @return
	 */
	int updateByPrimaryKeySelective(T entity);
	
	/**
	 * 根据主键查询
	 * 
	 * @param id
	 * @return
	 */
	T selectByPrimaryKey(String id);

	/**
	 * 查询数量
	 * 
	 * @param record
	 * @return
	 */
	int selectCountBySelective(T entity);

	/**
	 * 条件查询
	 * 
	 * @param map
	 * @return
	 */
	List<T> selectBySelective(T entity);
	
}
